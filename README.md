# Primer Workshop DataHack Formation

## Temas
1. Procesamiento en tiempo real usando Apache Druid y Superset
1. How_I_Could_Pass_The_Google_Cloud_Certification_Challenge
1. DataHackFormation_Program

## Contáctanos
Si te gustó el tema o quieres conversar sobre Data y Cloud escríbenos por 

[Facebook](https://www.facebook.com/datahackformation)  o WhatsApp +51 980518059